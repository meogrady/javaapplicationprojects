package com.appsense.mobilenow;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Practice {

	public static void main(String[] args) {
		/*System.out.println ("the Nth fibonacci number is: " + fibN (8));
		System.out.print ("fibonacci series is: "); fib (8);
		System.out.println ();
		maxString ();
		System.out.println ();
		char[] c = {'a', 'b', 'c', 'd'};
		permute (c, 4);*/
	}
	
	// output Nth fibonacci number
	public int fibN (int n) {
		if (n < 2)
			return n;
		return fibN (n-2) + fibN (n-1);
	}
	
	// output fib series
	public void fib (int n) {
		int a = 0;
		int b = 1;
		int result;
		while (a <= n) {
			System.out.print (" " + a);
			result = a + b;
			a = b;
			b = result;
		}
	}
	
	// get longest string
	public void maxString () {
		String str = "";
		int maxLen = -1;
		Scanner sc = null;
		try {
			sc = new Scanner (new File ("file.txt"));
			while (sc.hasNext()){
				String temp = sc.next();
				if (temp.length() > maxLen){
					str = temp;
					maxLen = temp.length ();
				}	
			}
			System.out.println ("Longest word is: " + "'" + str + "'" + " and it's length is " + maxLen);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			sc.close ();
		}
	}
	
	private void permute(char[] a, int n) {
        if (n == 1) {
            System.out.println(a);
            return;
        }
        for (int i = 0; i < n; i++) {
            swap(a, i, n-1);
            permute(a, n-1);
            swap(a, i, n-1);
        }
    }  
	
	// swap the characters at indices i and j
    private static void swap(char[] a, int i, int j) {
        char c;
        c = a[i]; a[i] = a[j]; a[j] = c;
    }

}
